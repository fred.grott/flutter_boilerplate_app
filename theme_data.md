# ThemeData Notes

colors, note alos now takes a colorScheme

primarySwatch
primaryColor
primaryColorLight
primaryColorDark

accentColor secondary
secondaryHeaderColor

ColorScheme Dark
background black
surface dark grey
onBackground white
onSurface white
onPrimary is now black
onSecondary is now black
have to set brightness on both colorSchemes ie dakr and light themes




TextThemes

primaryTextTheme
accentTextTheme

IconThemes

primaryIconTheme
accentIconTheme