# Flutter Boilerplate App

To work out my pixel perfection set of strategies including 1-to-1 scaling 
of widgets per screen-size, more advanced theme-ing for design constency 
across ios to android, devops classes for app execption catching, 
proper log in debug and not production, visual design polishes such as 
transparent status bar, and other visual polish.

## Why?

Because, while I could visually design some eye candy quickly and have it 
entice you the startup cofounder; its not a real good move in that the 
oop work to solve specific design problems should be close to completed first.
This way, you get what you pay for expert flutter internals knowledge 
re-used via oop to get pixel perfection in both the responsive UI laytouts,
typography, etc.

## Vids

[! Android Light and dark ](/media/mq2.jpg)](
https://www.youtube.com/watch?v=uwXid0AqV2I)

## Credentials

### Flutter_Platform_Widgets

I joined this project in January 2020 and started contributing code. Its a community efffort to polish up Google's Theme apparoch to dynamically deliver material loook to android and Apple Flat Design look to ios.

https://github.com/aqwert/flutter_platform_widgets

You can see how popular it is in the Flutter Community here at this link(score of 98 including code QA):

https://pub.dev/packages/flutter_platform_widgets#-analysis-tab-


### Previous Android Native(java-kotlin)

Just reusing my oop an design skills to rock flutter. Since I already knwo the rendering system used and the oop Google moved toward its real easy for me to plot a well designed and engineered course towards app pixel perfect perfectionusing flutter.

# Contact

fred DOT grott AT gmail DOT com

## Social

AngelList
https://angel.co/fred-grott



DesignHacksATYoutube-NO-Ads(I put mydesign progrees here)
https://www.youtube.com/channel/UCBRREUKFNWIELQ3oBzeXiMA?view_as=subscriber

Gitlab
https://gitlab.com/fred.grott



Medium
https://medium.com/@fredgrott

Twitter
https://twitter.com/fredgrott


LinkedIN
https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/
