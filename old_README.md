![android](/media/small-screenshot-2019-12-01_08.40.48.395.png)![ios](/media/small-screenshot-2019-12-01_08.44.49.684.png)
# flutter_boilerplate_app

Basically, this is my boilerplate code for fast prototyping flutter mobile apps. The other tool,
storiesofbook, is still being hacked out internally as storybook impl in dart-flutter.


## Vids

[![vid](/media/flutterbopilerplateapp.jpg)](https://www.youtube.com/watch?v=sbuoh0LVXuY "fluuter boilerplate app")

## Screenshot Collage

# Table Of Contents
1.[The Cofounder's Take-Away](#The Cofouonder's Take-Away)
2.[Tech Details](#Tech Details)
3.[Contact](#Contact)

## The Cofounder's Take-Away

Each cross platform front-end framework has its flaws and roudh spots. In Google's Flutter Framework case,
its using inherited widgets for dependency injection. While the widget dependency injector pattern does 
in fact have merit as being easier for both new devs and seasoned devs it has some performance issues
in that its not fully ooptimized.

I use my oop experience to split the problem into using singleton factory globals for app globals and 
state rebuilder dependency injection and state via reactive streams to still use the widget dependency 
injection pattern with react streams instead. The result is a very UI responsive to App user app 
with noticeable fast speed.

Further on the UX side, I use the state rebuilder dependency injector pattern further to do implicit 
animation constructs which means I can do more complex animation easily. Further, I am developing a
storybook-react screen user case isolation tool to fast prototype UI screens(WIP).

You will also notice in viewing the video that I excell on having a polished ios look on ios devices and 
a polished android look on android devices. Given, that Material Widgets and Cupertino WIdgets being 
separate approahces is onnly a year old, most flutter devs have yet ot master such feats.

## Summary In Short Words, Like you Are 5

* iOS look for ios devices, Android look for android devices
* Using expert OOP to fine tune the OOP approach on Flutter Apps to fine tune App performance around Flutter's rough spots
* Two weeks pre-written app code fully debugged and it works
* Dark Mode implemented for Android 10-and-up and iOS
* Dynamic font loading for a real complete internationalization solution figured out and WIP being implemented




## Tech Details

This is my own dev docs to my self and probably would only interest the CTO-cfoudner of your startup.
Or, in short words warning heavy tech words ahead!

### Dependency Injection

Google's Flutter Framework has a rough spot in that its inherited widget as dependency injection 
strategy with use of provider has performance issues as the Ui widget tree gets large with 
lots of nested widgets. My solution is to use two different dependency injection strategies.

### Widget Plus React Stream Injection

I use a widget plus react stream injection pattern using a service locator startegy via 
the states-rebuilder plugin, see:

https://pub.dev/packages/states_rebuilder

### Simple Dependency Injection

I also use a simple factory dependency injector pattern for my app globals via the 
flutter_simple_dependency plugin, see:

https://pub.dev/packages/flutter_simple_dependency_injection

###  OOP

Oh what a mess OOP still is.

#### Clean Arch

I implement a Clean Arch OOP to get a full separation of concerns to make testing 
easier and tomake it extremely easy to implement the Flux uni-directional pattern.

See Uncle Bob's original post:

https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

#### Flux

Facebook came up with an App arch uni-directional oop patter called flux:

https://facebook.github.io/flux/

I use Google's beta impl, see:

https://pub.dev/packages/flutter_flux

#### TDD

Test Driven Development is just the standard write a unit test and than write the impl and 
test until it passes the unit test with green.

#### BDD

For behavior driven development via user case testing, I use cucumber-gherkin to 
write case tests, steps, and features to test areas of concers such as
wigets using instrumented on device or simulator testing.

I use the flutter_gherkin plugin for this feature, see:

https://pub.dev/packages/flutter_gherkin

##### Storybook Use Case Screen Isolation Testing

This a wokr-in-process(WIP) as I am developing a plugin internally to re-implement the 
react-storybook library in dart-flutter.

### DevOops

The crazy world of devops.

#### Build Variants

While Google and other big players deliver services using mobile app and app individually 
specified keys to protect them often smaller players of api services do not. Thus, I have 
to use build variants to protect those keys and prevent them from being stored in the 
git code repositories.

I combine it with factorydependency injection to set build-variant variables.

#### Catching App Exceptions

I use a future async block to set up a zone so that when an app exception occurs it does not 
crash the app. Within the future-async block I use the cacther plugin to catch and report errors, see:

https://pub.dev/packages/catcher

#### Logging

One of my pet peeves from my Android experience is keeping logs out of production apps. While Apple 
has a nice way to keep logs out of production ios apps, Google does not. Thus, I use the fimber logging 
plugin and some assert-if-than code to detect debug mode to set up debug logging. See:

https://pub.dev/packages/fimber

To further frustrate hacker efforts a reverse engineering wholes I only use flutter IDE-console 
logging rather than log errors to the mobile-OS-on-device logging system.

#### Code Obfuscation

While TSL-1.3 makes it harder to sniff api keys its not fool=proof. Even though code obfuscation is 
alpha in flutter I do use it to complete the steps to lock down the app code from hacker 
intrusions.

see:

https://github.com/flutter/flutter/wiki/Obfuscating-Dart-Code

https://stackoverflow.com/questions/50542764/how-to-obfuscate-flutter-apps

#### Dynamic Deliver iOS-Cupertino Widgets To iOS And Material Widgets To Android

Google while separating out ios look to Cupertino Widgets and Android Material Widgets, 
has not fully developed heir oown plugin to make it easier. So I have to implement 
more widget instrumented testing due to the alpha plugin I use to do sqid feats. The 
plugin I use is the flutter-platform_widgets plugin, see:

https://pub.dev/packages/flutter_platform_widgets

As I have stated, I have the extra burden of writing extra widget instrumented tests because
the plugin currently has not implemented them. I am in the process of obtaining some 
flutter devs to help me congtribute instrumented widget testing for the flutter-pplatform_widgets
plugin project.

##### Dark and Light Mode

One of the benefits of Flutter is that certain features are fully implemented in all android OS 
version targets. This includes dark and light modes. I use flutter platform widgets for this 
feature as I can easliy abstract it out and set up user settings.

##### Full Screen



One, at this point of Jan 2020, still has to implement full screen as a native code 
modification of the flutter view wraper per each platform target build as of 
flutter sdk version 1.12.

see:



## Contact

Hello, this is Fred Grott and I might be your potential Flutter Mobile Expert for your startup.
To contact me use my gmail addy which is:


fred DOT grott AT gmail DOT com

Please, obviously no recruiters as those follks do not have what I am seeking.






 

# Hi Product direcors nd CPOs

The unasked questions you have about flutter, oop, dart code, etc should go unanswered as your end cusomters want these butter smooth rending apps and flutter framework delivers and so do I.
Engage and ask me.

# Screenshots Collage


# Implementation

## Security

I have to secure code and api keys in a speicfic these specific ways:

### CI Server Build and Production Builds

Both builds when I finish the secure api keys impl require a firebase services json file with Dis in the app modules and that set of files obvously cannot be in any git rpeo that is in the cloud so I will probable have to use a local mac box to do these builds and use bsh scripting to get a local docker perpared image of dev sdk tools that also stores the json file set in it.

### Preventing Software Engineering

I have to enabgle the Alpha flutter-tools obfuscate feature, see:

accoding to 
https://github.com/flutter/flutter/wiki/Obfuscating-Dart-Code

I have to

```
Android
Add the following line to <ProjectRoot>/android/gradle.properties:

extra-gen-snapshot-options=--obfuscate
```

and

```
iOS
Step 1 - Modify the "build aot" call
Add the following flag to the build aot call in the <FlutterRoot>/packages/flutter_tools/bin/xcode_backend.sh file:

${extra_gen_snapshot_options_or_none}
Define this flag as follows:

local extra_gen_snapshot_options_or_none=""
if [[ -n "$EXTRA_GEN_SNAPSHOT_OPTIONS" ]]; then
  extra_gen_snapshot_options_or_none="--extra-gen-snapshot-options=$EXTRA_GEN_SNAPSHOT_OPTIONS"
fi
Step 2 - Modify the release config
In <ProjectRoot>/ios/Flutter/Release.xcconfig, add the following line:

EXTRA_GEN_SNAPSHOT_OPTIONS=--obfuscate
```

### Using Firebase and secure storage to store api keys

* Per fall 2019 changes in TSL libs used by browsers, web servers, and mobile devices one can no longer effectively use wire sniffing software to sniff out api keys in https traffic, which I did verify its correct!
* There is a secure storage plugin to use the mobile hardware key chain to store keys
* firebase remote config can be used to store these keys and values despite the security wanring in docs as that warning was base don the previous versions of TSL libs in both devices and web browers. See:

https://pub.dev/packages/firebase_remote_config

and https://github.com/mogol/flutter_secure_storage

Note, its AN IMPERFECT SOLUTION and no way qurantees no illegal hacking to get api keys it onloy severly slows it down for 100 percent of those that try. There are green devs and esigners that are being picky becuase they do not understnad the diffculty of doing a prox of a mobile app in the first palce in sniffing https under TSL 1.3 combined with the other stpes in security of obfuscating the binary, etc.

## Testing and Production , Screenshots

We need screen shots for the acceptance testing and to take  some screen shots when we do a production builds. I amusing the flutter screenshots util software, see:


https://github.com/mmcc007/screenshots

and can set via a screenshots.yaml file to also usee device frames to insert the image into.


## build variants impl





build variants are

main_dev

main_prod

main_staging

acceptance_app

Note, acceptance_app is the test_driver folder and set up as we need to still
supply vars for theprudction app during acceptance testing while at the 
same time its gitnored to prevent api keys form being stored in the 
git repo.

NEVEr EVER build the apk using android studio commands.
Always run build apk flutter command from command line and use
the -t target buildvariants/buildvariantname.dart instead.

constants.dart is the only file that needs to be gitignored 
to keep 3rd party keys out of the git repo

To use COnstants.constantname

Oh yeah notice that I only have to configure one damn mainDelegate instead of 
multiple mains in each build variant. Yeah I'm special.

## Flutter build_runner

We can set a build.yaml file to have such things as unit test automaticaly executed per build. the defsof the build.yamlspec is here at:

https://github.com/dart-lang/build/blob/master/docs/build_yaml_format.md

And explaination detials are here:

https://pub.dev/packages/build_runner





## logging impl

I use fimber instead of finber plus fimber-flutter to log stuff ot the android studio logviewer in the flutter project run console setup as I run both android and ios emulators via that setup rather than log stuff to the mobile hardware log outputs.

Logging is debug wrapped in the app init code block and verified to be in debug mode by checkign the flutter foundations lReleqse value as not every target of flutter has a dart vm as the web target does not.


Logging is via

```
 Fimber.d("DEBUG");
  Fimber.v("VERBOSE");
  Fimber.w("WARN");
```

see 
https://github.com/magillus/flutter-fimber

Because, fimber at its base does use the logging of flutter one can use some of the log
extensions such as:

https://pub.dev/packages/logger_flutter#-installing-tab-

Once installed, than one can include a widget in thedebug main tree to 
show the debug console log in the app upon shake.

## Catcher impl

This time around will be using catcher for all app exceptions.
I use the manual email handler so that the app user is aware of that a creash report is 
being asekd to be sent.

see

https://medium.com/flutter-community/handling-flutter-errors-with-catcher-efce74397862

https://github.com/jhomlala/catcher

https://stackoverflow.com/questions/57879455/flutter-catching-all-unhandled-exceptions



## Exception Handling Zones

Remember, in zones during dev mode app will not crash as they are reported as non fatal as that is one 
of purposes of runZoned in the first place.


## BDD impl

Run via dart command line

dart test_driver/app_test.dart

## User Stories

Things are too alpha for both storybook and dash book, but if I had to pick 
if it was beta it would be storybook. Soemthing to fix later.



## full screen

Still not fixed on android. need touse native solution on android.

Setting of

```
SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
```

in widget.build before the return

According to this 
https://github.com/flutter/flutter/issues/23913

messes up on Android due to padding in safe areas.

If I abuse the safe areas widget I probalby can get away with a reasonable fix on android, see:

https://stackoverflow.com/questions/49227667/using-safearea-in-flutter

In other words I need to set safeareas top and bottom to fales and the rest true just for android.

For now just modify the flutter activity clas sin android to something like:

```
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import io.flutter.app.FlutterActivity
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity(): FlutterActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN)
    GeneratedPluginRegistrant.registerWith(this)
  }
}
```
## Fonts

Google pluign to get google fonts, its aplha

see

https://github.com/material-foundation/google-fonts-flutter

Warning, no support of dynamic loading of fonts per locale to avoid the
1 gig download to support non western locacles. yet.


## Dynamically deliver Android look to android users and Apple cupertinao look to ios and mac users

I use flutter_platform_widgets to dynamically deliver android to android and ios look to ios users, see:

https://github.com/aqwert/flutter_platform_widgets

### Dark and Light Teme Modes

Not fully implemented yet but moving towards coding a theme manager and theme changer widget for this app example fully integrated with the flutter platform widgets plugin works.

## uuid

I use the uuid plugin, see

https://github.com/Daegalus/dart-uuid

## Dependency Injection

I use factory injection instead of inherited widget DI due to performance concerns of inherited widgets as facor injection is
faster. See

https://github.com/jonsamwell/flutter_simple_dependency_injection

Eventually, when Google finally realizes thaqt ddart cannot be further optimized for inherited widget di than hopefully 
Google will update and start suporting their inject library for dart-flutter, 

see:

https://github.com/google/inject.dart

## oop

Not using Bloc or Provider as inherited widget still has performance problems. 
So, for DI I am using factory injection and for state management and injection 
I am using Redux.

## Redux

see

https://pub.dev/packages/redux
https://pub.dev/packages/redux_dev_tools
https://pub.dev/packages/flutter_redux_dev_tools
https://pub.dev/packages/reselect
https://pub.dev/packages/redux_persist
https://pub.dev/packages/flutter_redux

flipperkit is not updated to redux 4 so leaving offf for now

## Gerkin setup for BDD

deps are:

https://pub.dev/packages/flutter_gherkin
https://pub.dev/packages/gherkin
https://pub.dev/packages/meta
https://pub.dev/packages/glob

and flutter_driver and flutter_test



# Contact

For non recruiter inquiries my addy is

fred DOT grott AT gmail DOT com

