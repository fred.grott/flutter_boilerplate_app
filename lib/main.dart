// original example code from flutter_platform_widgets plugin
// originally licensed MIT by Lance Johnstone
// My modifications licensed under BSD clause 2
import 'dart:async';
//import 'package:data_connection_checker/data_connection_checker.dart';

import 'package:flutter/cupertino.dart'show
CupertinoActionSheet,
CupertinoActionSheetAction,
CupertinoIcons,
CupertinoThemeData;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' show ButtonTextTheme, ButtonThemeData, Colors, DefaultMaterialLocalizations, Icons, TextTheme, Theme, ThemeData, ThemeMode;
import 'package:flutter/services.dart' show SystemChrome, SystemUiOverlay, SystemUiOverlayStyle;
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:fimber/fimber.dart';

import 'package:catcher/catcher_plugin.dart';
import 'package:flutter_boilerplate_app/responsive_utils.dart';
import 'package:flutter_boilerplate_app/themes_colors.dart';
import 'package:flutter_boilerplate_app/themes_textstyles.dart';

import 'package:flutter_boilerplate_app/ui/tabbed/basic_tabbed_page.dart';

import 'package:flutter_boilerplate_app/ui/tabbed/original_tabbed_page.dart';
import 'package:flutter_boilerplate_app/ui/tabbed/sliver_tabbed_page.dart';
import 'package:flutter_boilerplate_app/ui/tabbed_page.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
//import 'package:simple_animations/simple_animations/controlled_animation.dart';
//import 'package:simple_animations/simple_animations/multi_track_tween.dart';



import 'widget_themes.dart';
// ignore: directives_ordering
import 'dependencies_injecting.dart';
import 'ui/icon_page.dart';
import 'ui/my_list_view.dart';
import 'ui/my_list_view_header_page.dart';





bool result;



// I can put my global di singleton injects here
// ignore: prefer_void_to_null
Future<Null> mainDelegate() async{

  // get connection, are we internet connected/


  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (myAppInjector.get<AppDebugMode>().isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  if (myAppInjector.get<AppDebugMode>().isInDebugMode) {

  } else {
    // not in docs but in the fimber class itself at
    // https://github.com/magillus/flutter-fimber/blob/master/fimber/lib/fimber.dart
    final List<String> myLogLevels = ["D", "I", "W", "E"];
    Fimber.plantTree(DebugTree(useColors: true, logLevels:  myLogLevels, printTimeType: 1));
  }
  // per this so answer https://stackoverflow.com/questions/57879455/flutter-catching-all-unhandled-exceptions
  await runZoned<Future<void>>(
          () async {

        final CatcherOptions debugOptions =
        CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
        final CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy])
        ]);
        CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy],
              enableDeviceParameters: true,
              enableStackTrace: true,
              enableCustomParameters: true,
              enableApplicationParameters: true,
              sendHtml: true,
              emailTitle: (myAppInjector.get<AppName>().myAppName) + " Exceptions",
              emailHeader: "Exceptions",
              printLogs: true)
        ]);
        CatcherOptions(DialogReportMode(), [
          ToastHandler(
              gravity: ToastHandlerGravity.bottom,
              length: ToastHandlerLength.long,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              textSize: 12.0,
              customMessage: "We are sorry but unexpected error occured.")
        ]);
        CatcherOptions(DialogReportMode(), [ConsoleHandler(enableApplicationParameters: true,
            enableDeviceParameters: true,
            enableCustomParameters: true,
            enableStackTrace: true)]);


        Catcher(Main(), debugConfig: debugOptions, releaseConfig: releaseOptions);


      }, onError: (dynamic error, dynamic stackTrace) {
    Catcher.reportCheckedError(error, stackTrace);
  });

}

class Main extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) => App();
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {






  Brightness brightness = Brightness.light;

  @override
  Widget build(BuildContext context) {

    // hmm do I ned anootated region?
    // seems to work but eventually should move to using
    // annotedregion as I have nested widgets and diff appbars
    // per platforms
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values) ;
    // this for full screen
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      systemNavigationBarIconBrightness: Brightness.light
    ));


    // need to add appbar themes for both
    // materialapp and cupertinoapp
    // as of 1-30-2020 still have to set
    // fontFamily in material theme for fonts
    // on all platforms ie no set font for iios separate
    // from android
    final materialTheme = ThemeData(
      // as of 2-7-2020 I still need to set this for appbar color
      primaryColor: Colors.purple,

      colorScheme: myLightColorScheme,
      textTheme: myMaterialTextTheme,
      buttonTheme: myButtonTheme,







    );
    final materialDarkTheme = ThemeData(
      brightness: Brightness.dark,


      colorScheme: myDarkColorScheme,
        textTheme: myMaterialTextTheme,


    );

    final cupertinoTheme =
    CupertinoThemeData(
      // work-around current CuperinoApp using defualt roboto fonts
      // and google_fonts pluginnot realy ready for beta
      // other part is defining fonts in pubspec
      // backout not ready yet
      // still cannot set fonts 1-30-2020 when using the
      // flutter_platform_widgets plugin but not usre if its that
      //plugin's falut
      //textTheme: const CupertinoTextThemeData(
      //textStyle: TextStyle(fontFamily: 'WorkSans')
      //),



      brightness: brightness, // if null will use the system theme
      primaryColor: primaryPurple,



      barBackgroundColor: barBackgroundPrimary,
      textTheme: myCupertinoTextTheme



    );

    // Example of optionally setting the platform upfront.
    //final initialPlatform = TargetPlatform.iOS;

    // If you mix material and cupertino widgets together then you cam
    // set this setting. Will mean ios darmk mode to not to work properly
    //final settings = PlatformSettingsData(iosUsesMaterialWidgets: true);

    // This theme is required since icons light/dark mode will look for it
    return Theme(
      data: brightness == Brightness.light ? materialTheme : materialDarkTheme,
      child: PlatformProvider(
        //initialPlatform: initialPlatform,
        builder: (context) => PlatformApp(
          // so I can do vid recording of debug runs without the banner
          debugShowCheckedModeBanner: false,
          navigatorKey: Catcher.navigatorKey,
          localizationsDelegates: <LocalizationsDelegate<dynamic>>[
            DefaultMaterialLocalizations.delegate,
            DefaultWidgetsLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          title: 'Flutter Platform Widgets',
          android: (_) {
            return MaterialAppData(
              theme: materialTheme,
              darkTheme: materialDarkTheme,
              themeMode: brightness == Brightness.light
                  ? ThemeMode.light
                  : ThemeMode.dark,
            );
          },
          ios: (_) => CupertinoAppData(
            theme: cupertinoTheme,
          ),
          home: LandingPage(() {
            setState(() {
              brightness = brightness == Brightness.light
                  ? Brightness.dark
                  : Brightness.light;
            });
          }),
        ),
      ),
    );
  }



}
class LandingPage extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  LandingPage(this.toggleBrightness);

  final void Function() toggleBrightness;

  @override
  LandingPageState createState() => LandingPageState();
}

class LandingPageState extends State<LandingPage> {
  @override
  // ignore: type_annotate_public_apis, always_declare_return_types
  initState() {
    super.initState();

    textControlller = TextEditingController(text: 'text');
    textMultiLineControlller = TextEditingController(text: 'text multi line text multi line text multi line text multi line text multi line'
        );

  }

  bool switchValue = false;
  double sliderValue = 0.5;

  TextEditingController textControlller;
  TextEditingController textMultiLineControlller;

  dynamic _switchPlatform(BuildContext context) {
    if (isMaterial(context)) {
      PlatformProvider.of(context).changeToCupertinoPlatform();
    } else {
      PlatformProvider.of(context).changeToMaterialPlatform();
    }
  }

  dynamic _toggleBrightness() {
    widget.toggleBrightness();
  }

  @override
  Widget build(BuildContext context) {
    // than I can call the proper
    // ResponsiveConfig.sizing in
    // my ui containers including correct font sizing

    ResponsiveConfig.init(context, allowFontScaling: true);

    // need to switch to
    // stack
    //   container
    // box decoration
    // content below appbar
    // positioned widget
    // appBar is child of the positioned widget
    // above this set body to new widget defintion
    // and have new widget def do what I need to do
    return PlatformScaffold(

      iosContentPadding: true,
      appBar: PlatformAppBar(
        title: const Text('Flutter Boilerplate', ),
        //backgroundColor: Colors.transparent,


        android: (_) => myMaterialAppBarData,


        // inmy litview pages need to set transparency
        ios: (_) => myCupertinoNavigationBarData,
        trailingActions: <Widget>[
          PlatformIconButton(
            padding: EdgeInsets.zero,
            iosIcon: Icon(CupertinoIcons.share),
            androidIcon: Icon(Icons.share),

            ios: (_) => myCupertinoIconButtonData,
            android: (_) => myMaterialIconButtonData,
            onPressed: () {},
          ),
        ],
      ),

      body: Container(
          width: double.infinity,
          height: double.infinity,

          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                  'Primary concept of this package is to use the same widgets to create iOS (Cupertino) or Android (Material) looking apps rather than needing to discover what widgets to use.'),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                  'This approach is best when both iOS and Android apps follow the same design in layout and navigation, but need to look as close to native styling as possible.'),
            ),
            const Divider(),
            PlatformButton(
              onPressed: _toggleBrightness,
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Toggle Dark / Light mode', style: TextStyle(color: Colors.white),),
            ),
            const Divider(),
            const SectionHeader(title: '1. Change Platform'),
            PlatformButton(
              onPressed: () => _switchPlatform(context),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Switch Platform', style: TextStyle(color: Colors.white),),
            ),
            PlatformWidget(
              android: (_) => const Text('Currently showing Material'),
              ios: (_) => const Text('Currently showing Cupertino'),
            ),
            const Text('Scaffold: PlatformScaffold', style: TextStyle(
                debugLabel: 'caption',
                fontFamily: 'Roboto',
                fontWeight: FontWeight.normal,
                fontSize: 12,
                wordSpacing: 0.4,
                fontStyle: FontStyle.normal
            ),),
            const Text('AppBar: PlatformAppBar', style: TextStyle(
                debugLabel: 'caption',
                fontFamily: 'Roboto',
                fontWeight: FontWeight.normal,
                fontSize: 12,
                wordSpacing: 0.4,
                fontStyle: FontStyle.normal
            ),),
            const Divider(),
            const SectionHeader(title: '2. Basic Widgets'),
            PlatformText(
              'PlatformText will uppercase for Material only',
              textAlign: TextAlign.center,
            ),
            PlatformButton(
              onPressed: () {},
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('PlatformButton', style: TextStyle(color: Colors.white),),
            ),
            PlatformButton(
              // ios flat stuff


              onPressed: () {
                setState(() {
                  const bool mystate = true;

                });
              },
              ios: (_) => myCupertinoButtonData,

              // need ios flat button stuff here
              androidFlat: (_) => myMaterialFlatButtonData,
              child: PlatformText('Platform Flat Button', style: TextStyle(color: Colors.white),),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: PlatformIconButton(
                androidIcon: Icon(Icons.home),
                iosIcon: Icon(CupertinoIcons.home),
                ios: (_) => myCupertinoIconButtonData,
                android: (_) => myMaterialIconButtonData,
                onPressed: () {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: PlatformSwitch(
                value: switchValue,
                onChanged: (bool value) => setState(() => switchValue = value),
                android: (_) => myMaterialSwitchData,
                ios: (_) => myCupertinoSwitchData,
              ),
            ),
            PlatformSlider(
              value: sliderValue,
              onChanged: (double newValue) {
                setState(() {
                  sliderValue = newValue;
                });
              },
              android: (_) => myMaterialSliderData,
              ios: (_) => myCupertinoSliderData,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: PlatformTextField(
                controller: textControlller,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              height: 100,
              child: PlatformTextField(
                expands: true,
                maxLines: null,
                controller: textMultiLineControlller,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: PlatformCircularProgressIndicator(ios: (_) => myCupertinoProgressIndicatorData,android: (_) => myMaterialProgressIndicatorData,),
            ),
            const Divider(),
            const SectionHeader(title: '3. Dialogs'),
            PlatformButton(
              onPressed: () => _showExampleDialog(),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Show Dialog',style: TextStyle(color: Colors.white),),
            ),
            const Divider(),
            const SectionHeader(title: '4. Popup/Sheet'),
            PlatformButton(
              onPressed: () => _showPopupSheet(),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Show Popup/Sheet',style: TextStyle(color: Colors.white),),
            ),
            const Divider(),
            const SectionHeader(title: '5. Navigation'),
            PlatformButton(
              onPressed: () => _openPage((_) => TabbedPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Open Tabbed Page',style: TextStyle(color: Colors.white),),
            ),
            PlatformButton(
              onPressed: () => _openPage((_) => BasicTabbedPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Open Basic Tabbed Page',style: TextStyle(color: Colors.white),),
            ),
            PlatformButton(
              onPressed: () => _openPage((_) => SliverTabbedPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Open Sliver Tabbed Page',style: TextStyle(color: Colors.white),),
            ),
            PlatformButton(
              onPressed: () => _openPage((_) => OriginalTabbedPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Open Original Tabbed Page',style: TextStyle(color: Colors.white),),
            ),
            const Divider(),
            const SectionHeader(title: '6. Icons'),
            PlatformButton(
              onPressed: () => _openPage((_) => IconsPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Open Icons Page',style: TextStyle(color: Colors.white),),
            ),
            const Divider(),
            const SectionHeader(title: '7. Advanced'),
            PlatformButton(
              onPressed: () => _openPage((_) => ListViewPage()),
              android: (_) => myMaterialRaisedButtonData ,
              ios: (_) => myCupertinoButtonData,
              child: PlatformText('Page with ListView',style: TextStyle(color: Colors.white),),
            ),
            PlatformWidget(
              android: (_) => Container(), //this is for iOS only
              // cannot use functions here so a manual copy
              ios: (_) => PlatformButton(
                onPressed: () => _openPage((_) => ListViewHeaderPage()),
                ios: (_) =>myCupertinoButtonData,

                child: PlatformText('iOS Page with Colored Header',style: TextStyle(color: Colors.white),),
              ),
            ),
          ],
        ),
      ),


    ));
  }

  dynamic _openPage(WidgetBuilder pageToDisplayBuilder) {
    Navigator.push<dynamic>(
      context,
      platformPageRoute<dynamic>(
        context: context,
        builder: pageToDisplayBuilder,
      ),
    );
  }

  dynamic _showPopupSheet() {
    showPlatformModalSheet<dynamic>(
      context: context,
      builder: (_) => PlatformWidget(
        android: (_) => _androidPopupContent(),
        ios: (_) => _cupertinoSheetContent(),
      ),
    );
  }

  Widget _androidPopupContent() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: const EdgeInsets.all(8),
              child: PlatformText('Option 1'),
            ),
          ),
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: const EdgeInsets.all(8),
              child: PlatformText('Option 2'),
            ),
          ),
        ],
      ),
    );
  }

  Widget _cupertinoSheetContent() {
    return CupertinoActionSheet(
      title: const Text('Favorite Dessert'),
      message:
      const Text('Please select the best dessert from the options below.'),
      actions: <Widget>[
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context, 'Profiteroles');
          },
          child: const Text('Profiteroles'),
        ),
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context, 'Cannolis');
          },
          child: const Text('Cannolis'),
        ),
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context, 'Trifle');
          },
          child: const Text('Trifle'),
        ),
      ],
      cancelButton: CupertinoActionSheetAction(
        isDefaultAction: true,
        onPressed: () {
          Navigator.pop(context, 'Cancel');
        },
        child: const Text('Cancel'),
      ),
    );
  }

  dynamic _showExampleDialog() {
    showPlatformDialog<dynamic>(
      context: context,
      builder: (_) => PlatformAlertDialog(
        title: const Text('Alert'),
        content: const Text('Some content'),
        ios: (_)=> myCupertinoAlertDialogData,
        android: (_)=>myMaterialAlertDialogData,
        actions: <Widget>[
          PlatformDialogAction(
            android: (_) => MaterialDialogActionData(),
            ios: (_) => CupertinoDialogActionData(),
            onPressed: () => Navigator.pop(context),
            child: PlatformText('Cancel'),
          ),
          PlatformDialogAction(
            onPressed: () => Navigator.pop(context),
            child: PlatformText('OK'),
          ),
        ],
      ),
    );
  }
}

class SectionHeader extends StatelessWidget {
  final String title;

  const SectionHeader({
    @required this.title,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        title,
        style: const TextStyle(
            debugLabel: 'headline6',
            fontFamily: 'RobotoCondensed',
            fontWeight: FontWeight.w500,
            fontSize: 20,
            wordSpacing: 0.15,
            fontStyle: FontStyle.normal
        ),
      ),
    );
  }
}

class Divider extends StatelessWidget {
  const Divider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1.0,
      color: const Color(0xff999999),
      margin: const EdgeInsets.symmetric(vertical: 12.0),
    );
  }
}

