// Orignal code from flutter_platform_widgets example
// Originaally licensed as MIT by Lance Johnstone
//
// my modifications licensed under BSD clausde 2 license
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class SubPage extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  SubPage(this.tab, this.level);
  final String tab;
  final int level;

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      iosContentPadding: true,
      appBar: PlatformAppBar(
        title: Text('$tab $level'),
      ),
      body: Column(
        children: [
          Text('Sub Page $tab with $level'),
          PlatformButton(
            onPressed: () => Navigator.push<dynamic>(
              context,
              platformPageRoute<dynamic>(
                context: context,
                builder: (context) => SubPage(tab, level + 1),
              ),
            ),
            child: const Text('Push to subpage',style: TextStyle(color: Colors.white),),
          ),
        ],
      ),
    );
  }
}
