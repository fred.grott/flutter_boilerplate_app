// Orignal code from flutter_platform_widgets example
// Originaally licensed as MIT by Lance Johnstone
//
// my modifications licensed under BSD clausde 2 license
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_boilerplate_app/ui/tabbed/pages/sub_page.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import '../../../widget_themes.dart';

class ContentView extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  ContentView(this.index);
  final int index;
  @override
  Widget build(BuildContext context) {
    print('_TabView $index');
    return Column(
      children: [
        PlatformButton(
          onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
          ios: (_) => myCupertinoButtonData,
          android: (_) => myMaterialRaisedButtonData,
          child: const Text('Back',style: TextStyle(color: Colors.white),),
        ),
        Text('Viewing Tab $index'),
        PlatformButton(
          ios: (_)=>myCupertinoButtonData,
          android: (_)=>myMaterialRaisedButtonData,
          onPressed: () => Navigator.push<dynamic>(
            context,
            platformPageRoute<dynamic>(
              context: context,
              builder: (context) {
                if (index == 0) {
                  return SubPage('Flag', 1);
                }
                return SubPage('Book', 1);
              },
            ),
          ),
          child: const Text('Push to subpage',style: TextStyle(color: Colors.white),),
        ),
      ],
    );
  }
}
