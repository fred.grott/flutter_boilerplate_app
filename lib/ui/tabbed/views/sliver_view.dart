// Orignal code from flutter_platform_widgets example
// Originaally licensed as MIT by Lance Johnstone
//
// my modifications licensed under BSD clausde 2 license
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class SliverView extends StatelessWidget {
  // ignore: prefer_const_constructors_in_immutables
  SliverView({
    @required this.title,
    @required this.children,
  });

  final String title;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        PlatformWidget(
          android: (context) => SliverAppBar(
            pinned: true,
            forceElevated: true,
            expandedHeight: 150.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(title),
            ),
          ),
          ios: (context) => CupertinoSliverNavigationBar(
            largeTitle: Text(title),
          ),
        ),
        SliverSafeArea(
          top: false, // Top safe area is consumed by the navigation bar.
          sliver: SliverList(
            delegate: SliverChildListDelegate(children),
          ),
        ),
      ],
    );
  }
}
