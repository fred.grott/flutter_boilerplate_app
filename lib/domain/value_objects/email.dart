import 'package:flutter_boilerplate_app/domain/exceptions/validation_exception.dart';
import 'package:meta/meta.dart';

@immutable
class Email {
  Email(this.email) {
    if (!email.contains('@')) {
      //Validation at the time of construction
      throw ValidationException('Your email must contain "@"');
    }
  }

  final String email;
}
