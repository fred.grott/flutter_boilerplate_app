// Copygith BSD Clause 2 2020 Fredrick Allan Grott

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


// note under Android Q I have to if than else some blocks by indiv widgets
// for systemgetstue insets detection


/*
   Initialize

   Responsive.init(context);

   Within the app with flutter_platfomr_wgdets pluing generally
   the first screen where the scaffold is ie first line of the build block
 */
class ResponsiveConfig{
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double _pixelRatio;
  static double _statusBarHeight;
  static double _bottomBarHeight;
  static double _textScaleFactor;
  static double blockSizeHorizontal;
  static double blockSizeVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;
  static double _paddingHeight;
  static double _paddingWidth;
  static double _physicalDepth;

  static ResponsiveConfig _instance;
  static const int defaultWidth = 1080;
  static const int defaultHeight = 1920;


  static num uiWidthPx;
  static num uiHeightPx;

  static bool allowFontScaling;

  factory ResponsiveConfig(){
    return _instance;
  }

  static void init(BuildContext context,{num width = defaultWidth,
      num height = defaultHeight,
  bool allowFontScaling = false}){
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    _pixelRatio = _mediaQueryData.devicePixelRatio;
    _statusBarHeight = -_mediaQueryData.padding.top;
    _bottomBarHeight = _mediaQueryData.padding.bottom;
    _textScaleFactor = _mediaQueryData.textScaleFactor;
    blockSizeHorizontal = screenWidth/100;
    blockSizeVertical = screenHeight/100;
    _safeAreaHorizontal = _mediaQueryData.padding.left +
        _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top +
        _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal)/100;
    safeBlockVertical = (screenHeight - _safeAreaVertical)/100;
    _paddingHeight = screenHeight - ((_mediaQueryData.padding.top + _mediaQueryData.padding.bottom) / 100.0);
    _paddingWidth = screenWidth - ((_mediaQueryData.padding.left + _mediaQueryData.padding.right) / 100.0);
    _physicalDepth = _mediaQueryData.physicalDepth;

  }

  static MediaQueryData get mediaQueryData => _mediaQueryData;

  static double get deviceScreenWidth => screenWidth;

  static double get deviceScreenHeight => screenHeight;

  static double get devicePixelRatio => _pixelRatio;

  static double get deviceStatusBarHeight => _statusBarHeight;

  static double get deviceBottomBarheight => _bottomBarHeight;

  static double get deviceTextScaleFactor => _textScaleFactor;

  static double get deviceBlockSizeHorizontal => blockSizeHorizontal;

  static double get deviceBlockSizevertical => blockSizeVertical;

  static double get deviceSafeAreaHorizontal => _safeAreaHorizontal;

  static double get deviceSafeAreavertical => _safeAreaVertical;

  static double get devicePaddingHeight => _paddingHeight;

  static double get devicePaddingVertical => _paddingWidth;

  static double get devicePhysicalDepth => _physicalDepth;

  static double get scaleWidth => screenWidth / uiWidthPx;

  static double get scaleHeight => screenHeight / uiHeightPx;

  static double get scaleText => scaleWidth;

  static num setWidth(num width) => width * scaleWidth;

  static num setHeight(num height) => height * scaleHeight;

  static num setSp(num fontSize, {bool allowFontScalingSelf}) =>
      allowFontScalingSelf == null
          ? (allowFontScaling
          ? (fontSize * scaleText)
          : ((fontSize * scaleText) / _textScaleFactor))
          : (allowFontScalingSelf
          ? (fontSize * scaleText)
          : ((fontSize * scaleText) / _textScaleFactor));
}