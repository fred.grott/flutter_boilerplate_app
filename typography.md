# Typography

Fonts is using the Material Design text type system spec.

# Headlines

## HeadLine1

Light 96 -1.5

## Headline2

light 60 -0.5

## headline3

regular 48 0

## headline4

regular 34 0.25

## healdine5

regular 24 0

## headline6

medium 20 0.15

## subtitle1

regular 16 0.15

## subtitle2

medium 14 0.1

## body1

regular 16 0.5

## body2

regular 14 0.25

## button

medium 14 app caps 1.25

## caption

regular 12 0.4

## overline

regular 10 all caps 1.5